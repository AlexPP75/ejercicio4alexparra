package org.cuatrovientos.ed;

public class Principal {

	public static void main(String[] args) {
		Product p0 = new Product("manzanas", 5, 12.1F);
		Product p1 = new Product("platanos", 50, 100.1F);
		Product p2 = new Product("peras", 2, 2.1F);
		Invoice factura = new Invoice("Alex");
		factura.add(p0);
		factura.add(p1);
		factura.add(p2);
		float precioTotal = factura.total();
		System.out.println("El precio total es "+precioTotal);
		factura.remouve(1);
		System.out.println("El nuevo precio es: "+factura.total());
	}

}
