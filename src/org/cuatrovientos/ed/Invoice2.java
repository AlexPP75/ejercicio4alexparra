package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Invoice2 {
	private String customer;
	public ArrayList<Product> listaproductos;
	public Invoice2(String customer) {
		super();
		this.customer = customer;
		listaproductos = new ArrayList<Product>();
	}
	public void add(Product p) {
		this.listaproductos.add(p);
	}
	public void remouve(int id) {
		this.listaproductos.remove(id);
	}
	public float total() {
		float resultado = 0;
		for (Product product : listaproductos) {
			resultado += product.total();
		}
		return resultado;
	}
}
